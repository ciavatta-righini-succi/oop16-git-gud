package org.gitgud.application.node;

import javafx.scene.layout.Pane;

/**
 * Represent a generic panel.
 */
public interface Panel {

    /**
     * @return the pane of the panel
     */
    Pane getPane();

}
