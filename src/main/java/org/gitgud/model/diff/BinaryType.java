package org.gitgud.model.diff;

/**
 * Indicates the binary type of a file.
 */
public enum BinaryType {

    /**
     * All binary types.
     */
    IMAGE, UNKNOWN;

}
