package org.gitgud.model.commons;

/**
 * The container of the info about an author.
 */
public interface Author {

    /**
     * @return The e-mail.
     */
    String getMail();

    /**
     * @return The name.
     */
    String getName();

}
