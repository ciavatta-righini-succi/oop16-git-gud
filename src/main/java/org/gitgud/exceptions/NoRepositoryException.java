package org.gitgud.exceptions;

/**
 * Must be throwed when an no repository is loaded.
 */
public class NoRepositoryException extends RuntimeException {

    private static final long serialVersionUID = 1L;

}
